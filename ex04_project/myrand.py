# -*- coding: utf-8 -*-

__author__ = 'Viel Jaren Heitmann'
__email__ = 'viel.jaren.heitmann@nmbu.no'



import random


class LCGRand:

    def _init_(self, seed):
        """
        Purpose: Define the seed
        :param seed: seed set in main section
        """
        self.seed = seed

    def rand(self):
        """
        Purpose: Generate random numbers
        :return: new random number based on previous random number
        """
        a = 7 ** 5
        m = 2 ** 31 - 1
        new_seed = a * self.rnd % m
        self.rnd = new_seed
        return self.rnd


class ListRand:
    def _init_(self, input_list):
        """
        Purpose: Define the list input
        :param input_list: list set in main section
        """
        self.number_list = input_list
        self.ind = 0

    def rand(self):
        """
        Purpose: Generate the random numbers
        :return: random number based on the list
        """
        try:
            return_element = self.number_list[self.ind]
            self.ind += 1
            return return_element
        except:
            raise RuntimeError('List has finished.')


if __name__ == '__main__':
    seed = 15
    random_number_lcg = LCGRand(seed)
    for _ in range(5):
        print('Random number generated: ', random_number_lcg.rand)

    input_list = [1, 2, 3, 4, 5]
    list_rand = ListRand(input_list)
    for element in input_list:
        print(list_rand.rand())