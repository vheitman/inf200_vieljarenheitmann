# -*- coding: utf-8 -*-

__author__ = 'Viel Jaren Heitmann'
__email__ = 'viel.jaren.heitmann@nmbu.no'



import random

class Walker:
    def _init_(self):
        """
        Purpose: Define position for home and samfunnet
        """
        self.pentagon = home
        self.position = start
        self.step_count = 0

    def move(self):
        """
        Purpose: Increase/decrease the step count
        """
        if random.randint(0,1) == 0:
            self.position +=1
        else:
            self.position -= 1
        self.step_count += 1


    def is_at_home(self):
        """
        :return: whether home is reached or not
        """
        if self.position == self.pentagon:
            return True
        else:
            return False


    def get_position(self):
        """
        :return: Position of the walker
        """
        return self.position

    def get_steps(self):
        """
        :return: step count of the walker
        """
        return self.step_count


if __name__ == '__main__':
    distances = (1, 2, 5, 10, 20, 50, 100)

    paths = []
    for distance in distances:
        path_length = []

        for walk in range(5):
            single_walk = Walker(0, distance)
            while single_walk.is_at_home() is False:
                single_walk.move()

            steps = single_walk.get_steps()
            path_length.append(steps)

        paths.append(path_length)

    for element in paths:
        print("Distance: {0:>10} --> Path lengths: {1}".format(
            distances[paths.index(element)], element))