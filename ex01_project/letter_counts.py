# -*- coding: utf-8 -*-

__author__ = 'Viel Jaren Heitmann'
__email__ = 'viel.jaren.heitmann@nmbu.no'


def letter_freq(txt):
    pass

if __name__ == '__main__':
    text = input('Please enter text to analyse:')

    frequencies = letter_freq(text)
    for letter, count in frequencies.items():
        print('{:3}{:10}'.format(letter, count))

