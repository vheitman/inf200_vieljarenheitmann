# -*- coding: utf-8 -*-

import random

__author__ = 'Viel Jaren Heitmann'
__email__ = 'viel.jaren.heitmann@nmbu.no'


class Gambler:
    def __init__(self, initial, total, p=0.5):
        """
        :param initial: initial amount the player owns
        :param total: total amount of money in the bank
        :param p: probability of winning
        """
        self.m = initial
        self.M = total
        self.p = p

    def is_broke(self):
        """Returns True if player is broke."""
        return self.m == 0

    def owns_all(self):
        """Returns True if player owns everything."""
        return self.m == self.M

    def play(self):
        """
        Flip coin and update wealth accordingly.
        """

        flip = random.random()

        if flip < self.p:
            self.m += 1
            self.M -= 1
        else:
            self.m -= 1
            self.M += 1


class GamblerSimulation:
    def __init__(self, initial, total, p, seed):
        """
        :param initial: initial amount the player owns
        :param total: total amount of money in the bank
        :param p: probability of winning
        :param seed: random generator seed
        """
        self.m = initial
        self.M = total
        self.p = p
        self.seed = random.seed(seed)

    def single_game(self):
        """
        Simulate single game.

        :returns: ruin, duration
        """
        player = Gambler(self.m, self.M, self.p)
        rounds = 0
        while not player.is_broke() or not player.owns_all():
            player.play()
            rounds += 1

        if player.is_broke():
            return 0, rounds
        else:
            return 1, rounds

    def run_simulation(self, num_games):
        """
        Run a set of games.

        :param num_games: number of games to simulate
        :returns: list of wins and list of losses, each with durations
        """
        win = []
        loss = []

        for _ in range(num_games):
            game = GamblerSimulation(self.m, self.M, self.p, self.seed)
            (result, rounds) = game.single_game()

            if result == 1:
                win.append(rounds)
            else:
                loss.append(rounds)
        return win, loss


if __name__ == '__main__':

    prob = [0.0, 0.1, 0.2, 0.4, 0.45, 0.49, 0.5, 0.9]  # probability for winning
    M = 100     # initial amount of money in bank
    m = 25      # initial amount of money the gambler has
    sd = 12345
    n = 20

    for i in prob:
        simulation = GamblerSimulation(m, M, i, sd)
        wins, losses = simulation.run_simulation(n)

        print('Probability: ', i)
        print('Times gambler broke bank: ', len(wins))
        print('Times gambler was ruined: ', len(losses))
        print('Rounds when gambler won:', wins)
        print('Rounds when gambler was ruined: ', losses)
