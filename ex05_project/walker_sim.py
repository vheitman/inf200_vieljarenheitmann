# -*- coding: utf-8 -*-

import random

__author__ = 'Viel Jaren Heitmann'
__email__ = 'viel.jaren.heitmann@nmbu.no'


class Walker:
    """
    Source: Hans E Plesser, NMBU_INF200_H18, ex04

    A random walker.
    A walker starts at a given position and has a home.
    """

    def __init__(self, start, home):
        """
        :param start: initial position of the walker
        :param home: position of the walker's home
        """
        self.x = start
        self.home = home
        self.steps = 0

    def get_position(self):
        """Returns current position."""

        return self.x

    def get_steps(self):
        """Returns steps taken so far."""

        return self.steps

    def is_at_home(self):
        """Returns True if walker is at home position."""

        return self.x == self.home

    def move(self):
        """
        Change coordinate by +1 or -1 with equal probability.
        """

        self.x += 2 * random.randint(0, 1) - 1
        self.steps += 1


class Simulation:
    def __init__(self, start, home, seed):
        """
        :param start: walker's initial position
        :param home: walk ends when walker reaches home
        :param seed: random generator seed
        """
        self.start = start
        self.home = home
        self.seed = random.seed(seed)

    def single_walk(self):
        """
        Simulate single walk from start to home, returning number of steps.

        :returns: number of steps taken
        """

        walk = Walker(self.start, self.home)
        while not walk.is_at_home():
            walk.move()

        return walk.get_steps()

    def run_simulation(self, num_walks):
        """
        Run a set of walks, returns list of number of steps taken.

        :param num_walks: number of walks to simulate
        :returns: list with number of steps per walk
        """
        steps = []
        for _ in range(num_walks):
            walk = Simulation.single_walk(self)
            steps.append(walk)

        return steps


if __name__ == '__main__':

    n = 2
    for i in range(n):
        walk1 = Simulation(0, 10, 12345)
        walk2 = Simulation(10, 0, 12345)

        print('Start: 0    Home: 10    Seed: 12345     Steps: ', walk1.run_simulation(20))
        print('Start: 10   Home: 0     Seed: 12345     Steps: ', walk2.run_simulation(20))

    walk3 = Simulation(0, 10, 54321)
    walk4 = Simulation(10, 0, 54321)
    print('Start: 0    Home: 10    Seed: 54321     Steps: ', walk3.run_simulation(20))
    print('Start: 10   Home: 0     Seed: 54321     Steps: ', walk4.run_simulation(20))
