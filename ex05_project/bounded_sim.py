# -*- coding: utf-8 -*-

from walker_sim import Walker, Simulation

__author__ = 'Viel Jaren Heitmann'
__email__ = 'viel.jaren.heitmann@nmbu.no'


class BoundedWalker(Walker):
    def __init__(self, start, home, left_limit, right_limit):
        """
        :param start: initial position of the walker
        :param home: position of the walker's home
        :param left_limit: left boundary of walker movement
        :param right_limit: right boundary of walker movement
        """
        super(BoundedWalker, self).__init__(start, home)
        self.ll = left_limit
        self.rl = right_limit

    def bounded_move(self):
        """
        Checks whether the limits has been reached, and adds a step but not a move if so
        :return:
        """
        position = self.get_position()
        if position <= self.ll:
            self.steps += 1
        elif position >= self.rl:
            self.steps += 1
        else:
            self.move()


class BoundedSimulation(Simulation):
    def __init__(self, start, home, seed, left_limit, right_limit):
        """
        :param start: walker's initial position
        :param home: walk ends when walker reaches home
        :param seed: random generator seed
        :param left_limit: left boundary of walker movement
        :param right_limit: right boundary of walker movement
        """
        super(BoundedSimulation, self).__init__(start, home, seed)
        self.ll = left_limit
        self.rl = right_limit

    def single_walk(self):
        """
        Simulation of walk until home, and with boundaries.
        :return: number of steps taken
        """
        walk = BoundedWalker(self.start, self.home, self.ll, self.rl)
        while not walk.is_at_home():
            walk.bounded_move()

        return walk.get_steps()

    def run_bound_sim(self, num_walks):
        """
        Run a set of walks, returns list of number of steps taken.
        :param num_walks: number of walks to simulate
        :returns: list with number of steps per walk
        """

        steps = []
        for _ in range(num_walks):
            walk = BoundedSimulation.single_walk(self)
            steps.append(walk)

        return steps


if __name__ == '__main__':
    l_lim = (0, -10, -100, -1000, -10000)
    for i in l_lim:
        simulation = BoundedSimulation(0, 20, 12345, i, 20)
        walks = simulation.run_bound_sim(20)
        print('Left limit: {:}  Steps: {:}'.format(l_lim, walks))
