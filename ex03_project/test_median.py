# -*- coding: utf-8 -*-

__author__ = 'Viel Jaren Heitmann'
__email__ = 'vheitman@nmbu.no'

'''
The median function gives the median of a list.
It is tested that it works on different kind of lists.
'''


def median(data):
    """
    Returns median of data.

    :param data: An iterable of containing numbers
    :return: Median of data
    """

    sdata = sorted(data)
    n = len(sdata)
    return (sdata[n//2] if n % 2 == 1
            else 0.5 * (sdata[n//2 - 1] + sdata[n//2]))


def test_single():
    # Testing median function on single entry list
    assert median([1]) == 1, 'Error: Median does not work on single element list'


def test_odd():
    # Testing median function on list with odd number of elements
    data = [6, 2, 7, 9, 2, 0, 1]
    assert median(data) == 2, \
        'Error: Median does not work on list with odd number of elements'


def test_even():
    # Testing median function on list with even number of elements
    data = [4, 7, 2, 3, 6, 0]
    assert median(data) == 3.5, \
        'Error: Median does not work on list with even number of elements'


def test_ordered():
    # Testing median function on a ordered list
    data = sorted([4, 7, 2, 3, 6, 0])
    assert median(data) == 3.5, \
        'Error: Median does not work on a sorted list'


def test_reverse():
    # Testing median function on a reverse-ordered list
    data = sorted([4, 7, 2, 3, 6, 0])
    data.reverse()
    assert median(data) == 3.5, \
        'Error: Median does not work on a reversed sorted list'


def test_unordered():
    # Testing median function on unordered list
    data = [4, 7, 2, 3, 6, 0]
    assert median(data) == 3.5, \
        'Error: Median does not work on unsorted list'


def test_empty():
    # Raising a ValueError if list is empty
    try:
        median([])
    except ValueError:
        print('Error: List is empty. No median found.')
    except IndexError:
        print('Error: List is empty. No median found.')


def test_original_unchanged():
    # Testing that the original list is unchanged.
    data = [4, 7, 2, 3, 6, 0]
    data_2 = data.copy()
    m = median(data)
    assert data == data_2, 'Error: The original list has been changed'


def test_tuples_lists():
    # Testing that median also works on tuples as well as lists
    l = [4, 7, 2, 3, 6, 0]
    t = (4, 7, 2, 3, 6, 0)
    assert median(t) == median(l) == 3.5, \
        'Error: Median is not same in a tuple as in a list'