# -*- coding: utf-8 -*-

"""
Program for asserting the bubble sort function on different data sets
"""

__author__ = 'Viel Jaren Heitmann'
__email__ = 'vheitman@nmbu.no'


def bubble_sort(in_data):
    s_data = list(in_data)
    for j in reversed(range(len(s_data))):
        for k in range(j):
            if s_data[k+1] < s_data[k]:
                s_data[k], s_data[k+1] = s_data[k+1], s_data[k]
    return s_data


def test_empty():
    """Test that the sorting function works for empty list"""
    assert bubble_sort([]) == [], 'Error: does not work on empty list'


def test_single():
    """Test that the sorting function works for single-element list"""
    assert bubble_sort([1]) == [1], 'Error: does not work on list with single element'


def test_sorted_is_not_original():
    """
    Test that the sorting function returns a new object.

    Consider

    data = [3, 2, 1]
    sorted_data = bubble_sort(data)

    Now sorted_data shall be a different object than data,
    not just another name for the same object.
    """
    data = [3, 2, 1]
    sorted_data = bubble_sort(data)
    assert id(data) != id(bubble_sort), \
        'Error: Function does not make a new list for sorting'


def test_original_unchanged():
    """
    Test that sorting leaves the original data unchanged.

    Consider

    data = [3, 2, 1]
    sorted_data = bubble_sort(data)

    Now data shall still contain [3, 2, 1].
    """
    data = [3, 2, 1]
    data_2 = data.copy()
    sorted_data = bubble_sort(data)
    assert data == data_2, 'Error: The original list is changed'


def test_sort_sorted():
    """Test that sorting works on sorted data."""
    data = sorted([4, 2, 7, 8])
    assert bubble_sort(data) == [2, 4, 7, 8], \
        'Error: Sorting does not work on sorted list'


def test_sort_reversed():
    """Test that sorting works on reverse-sorted data."""
    data = sorted([4, 2, 7, 8])
    data.reverse()
    assert bubble_sort(data) == [2, 4, 7, 8], \
        'Error: Sorting does not work on reverse-sorted data'


def test_sort_all_equal():
    """Test that sorting handles data with identical elements."""
    assert bubble_sort([1]*5) == [1]*5, \
        'Error: Sorting does not work on lists with identical elements'


def test_sorting():
    """
    Test sorting for various test cases.

    This test case should test sorting of a range of data sets and
    ensure that they are sorted correctly. These could be lists of
    numbers of different length or lists of strings.
    """
    import random

    n = random.randint(1, 10)
    data = []

    for _ in range(n):
        x = random.randint(1, 100)
        data.append(x)

    assert bubble_sort(data) == sorted(data), \
        'Error: The random generated list can not be sorted'
