# -*- coding: utf-8 -*-

'''
Ex02 - A: Statistics on files
Opens a file and counts the characters it contains. 
'''

__author__ = 'Viel Jaren Heitmann'
__email__ = 'viel.jaren.heitmann@nmbu.no'


def char_counts(textfilename):
    f = open(filename(), 'r').read() # opens filename in reading mode
    text = unicode(f)

    counts = {}
    for char in text.lower():
        if char in counts:
            counts[char] += 1
        else
            counts[char] = 1


if __name__ == '__main__':

    filename = 'file_stats.py'
    frequencies = char_counts(filename)
    for code in range(256):
        if frequencies[code] > 0:
            print('{:3}{:>4}{:6}'.format(code,
                                         chr(code) if code >= 32 else '',
                                         frequencies[code]))

