# -*- coding: utf-8 -*-


'''
Ex02 - C: Bubble sort
'''

__author__ = 'Viel Jaren Heitmann'
__email__ = 'viel.jaren.heitmann@nmbu.no'

def bubble_sort(data):

    length = len(data) - 1
    sorted = False

    while not sorted:
        sorted = True
        for element in range(0,length):
            if data[element] > data[element + 1]:
                hold = data[element + 1]
                data[element + 1] = data[element]
                data[element] = hold
                sorted = False
    return data


if __name__ == "__main__":

    for data in ((),
                 (1,),
                 (1, 3, 8, 12),
                 (12, 8, 3, 1),
                 (8, 3, 12, 1)):
        print('{!s:>15} --> {!s:>15}'.format(data, bubble_sort(data)))