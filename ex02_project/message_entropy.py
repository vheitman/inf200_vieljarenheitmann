# -*- coding: utf-8 -*-

'''
Ex02 - B: Entropy of a message
Counts numbers of letters, and calculates the entropy in message
'''

__author__ = 'Viel Jaren Heitmann'
__email__ = 'viel.jaren.heitmann@nmbu.no'


def letter_freq(string):
    """
    Count the number of times different character appears in a string.
    """

    counts = {}
    for char in string.lower():
        if char in counts:
            counts[char] += 1
        else:
            counts[char] = 1
    return counts


def entropy(message):
    text = msg
    frequencies = letter_freq(text)
    x = []

    for letter, count in frequencies.items():
        x = [letter, count]
        for i in range (count)
            H = - Sum\_i p[i] log\_2 p[i]

    return H



if __name__ == "__main__":
    for msg in '', 'aaaa', 'aaba', 'abcd', 'This is a short text.':
        print('{:25}: {:8.3f} bits'.format(msg, entropy(msg)))
